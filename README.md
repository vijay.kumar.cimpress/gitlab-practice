# GitLab-practice

Practice repository to get familiar with git and GitLab.

# Individual Contributions

Create separate branches under your names and add individual text files with some info in the folder "individual_contributions".
Raise a PR and then add reviewers. The purpose of this exercise is to understand the basic git terminologies and become familiar with branches, pull requests, code reviews, etc.

# Common Contribution

The common contribution goes in the file 'teamLegion.txt'. The purpose of this exercise is to understand and resolve conflicts that might arise when several people are working on the same file.
